#!/bin/bash

mkdir -p "forensic_log_website_daffainfo_log"

awk -F: 'NR>1 {count[$3]++} END {req = 0; hour = 0; for (i in count) {req = req + count[i]; hour++;}rph = req / hour; print "Rata-rata serangan adalah sebanyak", rph, "requests per jam"}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt

awk -F: 'NR>1 {count[$1]++} END {for (i in count) {printf "IP yang paling banyak mengakses server adalah: %s sebanyak %s requests\n", i, count[i] | "sort -r -nk10"}}' log_website_daffainfo.log | head -n 1 > forensic_log_website_daffainfo_log/result.txt

awk '/curl/ {count++} END {printf "\nAda %s requests yang menggunakan curl sebagai user-agent\n\n", count}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt

awk -F: 'NR>1 && $3==02 {count[$1]++} END {for (i in count) {printf "%s Jam 2 pagi\n", i}}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt