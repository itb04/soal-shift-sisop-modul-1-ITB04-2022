# soal-shift-sisop-modul-1-ITB04-2022
## Laporan Pengerjaan Soal Shift Modul 1 Praktikum Sistem Operasi
### Nama Anggota Kelompok:
 1. Hafizh Abid Wibowo - 5027201011
 2. Axellino Anggoro Armandito - 5027201040
 3. Anak Agung Bintang - 5027201060

<br>

# Soal 1
### a. Membuat sistem register (register.sh) dan sistem login (main.sh). Kemudian setiap user yang berhasil register disimpan ke dalam */users/user.txt*

<br>

``` sh
#!/bin/bash

mkdir -p ./users
test -f ./users/user.txt || touch ./users/user.txt
test -f log.txt || touch log.txt
```

<br>

- `mkdir -p ./users` 
  <br> 
  Digunakan untuk membuat *parent directory* bernama users

- `test  -f ./users/user.txt || touch ./users/user.txt` 
  <br>
Digunakan untuk mengecek apakah *file* user.txt sudah dibuat. Apabila belum ada, maka akan dibuat file user.txt

<br>

> Output nomor 1
![1a](img/1a.png)

Setelah script di run akan membuat folder baru bernama users yang terisi dengan user.txt

---

<br>

### b. Membuat input password yang tertutup untuk sistem login dan register dengan kriteria sebagai berikut:
- Minimal 8 karakter
- Memiliki minimal 1 huruf kapital dan 1 huruf kecil
- Alphanumeric
- Tidak boleh sama dengan username

<br>

``` sh
read -p "Password: " -s password

if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]]
```

<br>

- `read -p "Password: " -s password` 
  <br>
  Digunakan untuk menerima input password yang tertutup dari pengguna yang akan disimpan pada variabel *password*

- `if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]]`
  <br>
 Statement if ini secara berurutan akan memfilter supaya password sesuai dengan kriteria, yaitu tidak sama dengan username, memiliki >= 9 karakter, terdapat huruf kapital dan huruf kecil, dan terdapat karakter alphanumeric

<br>

> Output nomor 1b jika password sesuai kritera 
![sesuai kriteria](img/1b.png)
Register dengan username **axel** dan password **Asdf1234**

<br>

> Output nomor 1b jika password tidak sesuai kriteria
![sesuai kriteria](img/1b_gagal.png)

Register dengan username **asdf** dan password **asdf**

---

<br>

### c. Mencatat log login dan register dengan format MM/DD/YY hh:mm:ss **MESSAGE**. Dengan ketentuan message sebagai berikut:
1. Ketika mencoba register dengan username yang sudah terdaftar, maka message pada log adalah REGISTER: ERROR User already exists
2. Ketika percobaan register berhasil, maka message pada log adalah REGISTER: INFO User USERNAME registered successfully
3. Ketika user mencoba login namun passwordnya salah, maka message pada log adalah LOGIN: ERROR Failed login attempt on user USERNAME
4. Ketika user berhasil login, maka message pada log adalah LOGIN: INFO User USERNAME logged in

<br>
<br>

**Untuk ketentuan nomor 1**

``` sh
d=`date +%D`
t=`date +%T`

exist=$(awk -v username="${username}" '$1==username {print 1}' ./users/user.txt)

  if [[ "$exist" == 1 ]]
then
        echo "$d $t REGISTER: ERROR User already exist" >> log.txt
        exit
fi
```

<br>

- `d=date +%D`
  <br>
  Untuk menampilkan format waktu dalam bentuk *MM/DD/YY* yang disimpan dalam variabel *d*

- `t=date +%T`
  <br>
  Untuk menampilkan format waktu dalam bentuk *hh:mm:ss* yang akan disimpan dalam variabel *t*

- AWK akan menginisialisasi variabel lokal *username* yang diisi dengan value dari variabel global *username* dari input pengguna. Kemudian akan dilakukan pengecekan apakah username tersebut sudah berhasil diregister atau belum. Apabila ada, maka akan print 1 yang disimpan pada variabel *exist*. AWK disini membaca isi dari file **user.txt**

- Pada statement if disini akan menerima input 1 dari variabel *exist* sebelumnya dan akan menampilkan pesan "REGISTER: ERROR User already exist" apabila melakukan registrasi dengan username yang sama

<br>

> Output ketentuan 1

![ketentuan_1](img/1c_k1.png)

Register menggunakan username yang telah terdaftar

<br>
<br>

**Untuk ketentuan nomor 2**

```sh
if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]]

then
        echo "$d $t REGISTER: INFO User $username registered successfully" >> log.txt
        echo "$username $password" >> ./users/user.txt
        exit
else
        echo "$d $t REGISTER: ERROR password does not match the format" >> log.txt
        exit
fi
```

<br>

- Setelah password yang diinput sesuai kriteria, maka output `REGISTER: INFO User USERNAME registered successfully` akan disimpan pada **log.txt**. Sedangkan untuk username dan password akan disimpan di **user.txt**

- Namun, apabila password tidak sesuai dengan kriteria, maka output `REGISTER: ERROR password does not match the format` dan disimpan pada **log.txt**

<br>

> Output ketentuan nomor 2
![ketentuan_2](img/1c_k2.png)

Register menggunakan username **qwerty** dan password **1234**

<br>
<br>

**Untuk ketentuan nomor 3 dan 4**

``` sh
exist=$(awk -v username="${username}" -v password="${password}" '$1==username && $2==password {print 1}' ./users/user.txt)

if [[ "$exist" == 1 ]]
then
        echo "$d $t LOGIN: INFO User $username logged in" >> log.txt
        ...
        ...
else
        echo "$d $t LOGIN: ERROR Failed login attempt on user $username" >> log.txt
        exit
fi
```

<br>

- Dengan AWK insialisasi variabel lokal *username* dan *password* dimana keduanya diambil dari variabel global berdasarkan input pengguna. Kemudian akan diperiksa apakah di dalam **user.txt** terdapat username dan password yang sesuai. Apabila ada, maka print 1 dan disimpan ke dalam variabel *exist*

- Di statement if selanjutnya akan berjalan apabila variabel *exist* bernilai 1 dan akan menyimpan output "LOGIN: INFO User Username logged in" pada **log.txt**

- Namun apabila exist tidak bernilai 1 akan menyimpan output "LOGIN: ERROR Failed login attempt on user Username" pada **log.txt**

<br>

> Output ketentuan nomor 3
![ketentuan_3](img/1c_k3.png)

<br>

> Output ketentuan nomor 4
![Ketentuan_4](img/1c_k4.png)

---

<br>

### d. Membuat query yang terdiri dari 2 command sebagai berikut:

### 1. dl N ( N = Jumlah gambar yang akan didownload)

Untuk mendownload gambar dari https://loremflickr.com/320/240 dengan jumlah sesuai dengan yang diinputkan oleh user. Hasil download akan dimasukkan ke dalam folder dengan format nama YYYY-MM-DD_USERNAME. Gambar-gambar yang didownload juga memiliki format nama PIC_XX, dengan nomor yang berurutan (contoh : PIC_01, PIC_02, dst. ).  Setelah berhasil didownload semua, folder akan otomatis di zip dengan format nama yang sama dengan folder dan dipassword sesuai dengan password user tersebut. Apabila sudah terdapat file zip dengan nama yang sama, maka file zip yang sudah ada di unzip terlebih dahulu, barulah mulai ditambahkan gambar yang baru, kemudian folder di zip kembali dengan password sesuai dengan user.

### 2. att

Menghitung jumlah percobaan login baik yang berhasil maupun tidak dari user yang sedang login saat ini.

<br>
<br>

**Untuk command dl N**

```sh
  ...
  read command
        cmd=$(awk '{print $1}' <<< "$command")
        q=$(awk '{print $2}' <<< "$command")
        if [[ "$cmd" == "dl" ]]
        then
                sudo apt install zip unzip
                folder=`date +%F`"_$username"
                index=0
                if [[ ! -f "./$folder.zip" ]]
                then
                        mkdir -p "./$folder"
                else
                        unzip -P "$password" "./$folder.zip"
                        file=`ls "$folder" -r | head -n 1`
                        index=$(awk -F"_" '{print $2}' <<< "$file")
                fi
                for ((num=index+1; num<=index+q; num++))
                do
                        wget -O "$folder/PIC_$num" https://loremflickr.com/320/240
                done
                zip -r -P "$password" "$folder.zip" "$folder"
                rm -rf "$folder"
```

<br>

- ``cmd=$(awk '{print $1}' <<< "$command")`` dan ``q=$(awk '{print $2}' <<< "$command")``
  <br>
  Mengambil input user pada variabel *command* di AWK dan mengambil argumen pertama untuk disimpan pada variabel *cmd*. Kemudian untuk variabel *q* digunakan untuk menyimpan input user pada argumen kedua sebagai banyaknya gambar yang ingin di download 

- `` `folder=`date +%F`"_$username"` ``
  <br>
  Untuk membuat folder sesusai dengan format YYYY-MM-DD_USERNAME

- ``if [[ ! -f "./$folder.zip" ]]`` 
  <br>
  Di statement if ini akan diperiksa apakah sudah ada file dengan format YYYY-MM-DD_USERNAME.zip. Apabila belum ada file, folder akan dibuat. Namun, apabila sudah ada, maka file tersebut akan di unzip sesuai input password user. Kemudian file tersebut akan diurutkan berdasarkan index dengan angka terbesar

- ``for ((num=index+1; num<=index+q; num++))``
  <br>
  For loop ini digunakan untuk melakukan looping index pada nama file gambar sebanyak input pengguna yang ditampung pada variabel *num*. Kemudian setiap 1x loop akan di download 1 gambar dari website https://loremflickr.com/320/340 yang akan diberi nama PIC_*num*. Gambar tersebut kemudian akan di zip sesuai password user

<br>

> Output command dl n (n = 3)
![dl_n](img/1d_dln.png)
![gambar](img/1d_gambar.png)

<br>

<br>

**Untuk command att**

```sh
elif [[ "$cmd" == "att" ]]
        then 
                awk -v username="${username}" '$3=="LOGIN:" && ($6==username || $10==username) {++n} END {print "Login attempt sebanyak", n, "kali"}' log.txt
        fi
```

<br>

- Setelah AWK menerima username yang sudah diinput pengguna kemudian akan diperiksa pada file **log.txt** dengan memanfaatkan argumen ketiga, keenam, dan kesepuluh untuk memperoleh banyak percobaan login

<br>

> Output command att

![att](img/1d_att.png)

<br>

<br>

<br>

# Soal 2
### a. Membuat folder forensic_log_website_daffainfo_log

<br>

```sh
#!/bin/bash

mkdir -p "forensic_log_website_daffainfo_log"
```

<br>

- `mkdir -p "forensic_log_website_daffainfo_log"`
  <br>
  Digunakan untuk membuat *parent directory* bernama **forensic_log_website_daffainfo_log**

<br>

### b. Mencari rata-rata request per jam kemudian disimpan ke file ratarata.txt

<br>

``` sh
awk -F: 'NR>1 {count[$3]++} END {req = 0; hour = 0; for (i in count) {req = req + count[i]; hour++;}rph = req / hour; print "Rata-rata serangan adalah sebanyak", rph, "requests per jam"}' log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt
```

- `-F:`
  <br>
  Di bagian ini AWK akan menentukan field separator berupa titik dua

- `NR > 1`
  <br>
  Di bagian ini akan memerintahkan AWK untuk mulai membaca file **log_website_daffainfo.log** dari baris kedua 

- `count[$3]++`
  <br>
  Di bagian ini akan menghitung banyak jam (argumen ketiga) yang terdapat di log 

- `END {req = 0; hour = 0; for (i in count) {req = req + count[i]; hour++;}rph = req / hour`
  <br>
  Untuk menghitung rerata request per jam pertama perlu inisialisasi variabel *req* untuk jumlah  request dan variabel *hour* untuk banyak jam. Kemudian for loop digunakan untuk mendapatkan total request. Terakhir untuk mendapatkan hasil rata-rata jumlah request akan dibagi dengan banyak jam dan hasilnya akan ditampung pada variabel *rph*

- `log_website_daffainfo.log > forensic_log_website_daffainfo_log/ratarata.txt`
  <br>
  Setelah output di print sesuai format yang diminta, hasil output akan dikirim ke file **ratarata.txt** penggunaan `>` berfungsi agar output baru selalu di overwrite apabila sudah tertulis di file tersebut

<br>

> Output ratarata.txt
![rph](img/2_b.png)

<br>

### c. Menampilkan IP yang paling banyak mengakses 

<br>

``` sh
awk -F: 'NR>1 {count[$1]++} END {for (i in count) {printf "IP yang paling banyak mengakses server adalah: %s sebanyak %s requests\n", i, count[i] | "sort -r -nk10"}}' log_website_daffainfo.log | head -n 1 > forensic_log_website_daffainfo_log/result.txt
```

- `-F:`
  <br>
  Di bagian ini AWK akan menentukan field separator berupa titik dua

- `NR > 1`
  <br>
  Di bagian ini akan memerintahkan AWK untuk mulai membaca file **log_website_daffainfo.log** dari baris kedua 

- `count[$1]++`
  <br>
  Di bagian ini akan menghitung banyak IP (argumen pertama) yang terdapat di log 

- `END {for (i in count)`
  <br>
  Iterasi loop variabel count

- `sort -r -nk10`
  <br>
  Di bagian ini akan melakukan melakukan reverse sorting berdasarkan key **ke-10**

- `log_website_daffainfo.log | head -n 1 > forensic_log_website_daffainfo_log/result.txt`
  <br>
  Setelah output di print sesuai format yang diminta, hasil output akan dikirim ke file **result.txt** penggunaan `>` berfungsi agar output baru selalu di overwrite apabila sudah tertulis di file tersebut. `head -n 1` berfungsi untuk mendapatkan output line pertama.

<br>

### d. Menampilkan banyak request dengan user-agent curl

<br>

```sh
awk '/curl/ {count++} END {printf "\nAda %s requests yang menggunakan curl sebagai user-agent\n\n", count}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

- `/curl/ {count++}`
  <br>
  Digunakan untuk menghitung jumlah baris yang mengandung karakter spesifik "curl"

- `log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt`
  <br>
  Setelah output di print sesuai format yang diminta, hasil output akan dikirim ke file **result.txt** penggunaan `>>` berfungsi agar output yang sudah tertulis di file tersebut tidak ter-overwrite atau tertimpa.

<br>

### e. Menampilkan IP yang mengakses jam 2 pagi 

<br>

```sh
awk -F: 'NR>1 && $3==02 {count[$1]++} END {for (i in count) {printf "%s Jam 2 pagi\n", i}}' log_website_daffainfo.log >> forensic_log_website_daffainfo_log/result.txt
```

- `-F:`
  <br>
  Di bagian ini AWK akan menentukan field separator berupa titik dua

- `NR>1 && $3==02 {count[$1]++}`
  <br>
  AWK akan mulai membaca log setelah line 1 dan argumen 3 akan digunakan untuk memperoleh request yang mengakses website pada jam 2 pagi. Kemudian `{count[$1]++}` berfungsi untuk mendapatkan IP-nya

> Output result.txt
![result.txt](img/2_cde.png)

<br>

<br>

<br>

# Soal 3
### a. Memasukkan semua metrics ke file log metrics_{YmdHms}.log

```sh
#!/bin/bash

user=$(whoami)
mkdir -p /home/"$user"/log

file="metrics_"`date +%Y%m%d%H%M%S`".log"

mem=$(awk '$1=="Mem:" {printf "%s,%s,%s,%s,%s,%s", $2, $3, $4, $5, $6, $7}' <<< `free -m`)
swap=$(awk '$1=="Swap:" {printf "%s,%s,%s", $2, $3, $4}' <<< `free -m`)
dir=$(awk '{printf "%s", $1}' <<< `du -sh /home/"$user"`)

echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/"$user"/log/"$file"
echo "$mem,$swap,/home/$user/,$dir" >> /home/"$user"/log/"$file"

chmod 700 "/home/$user/log/$file"
```
- `user=$(whoami)`
  <br>
  Digunakan untuk mendapatkan username yang disimpan ke dalam variabel *user*

- `mkdir -p /home/"$user"/log`
  <br>
  Digunakan untuk membuat *parent directory* dengan username pengguna

- ``file="metrics_"`date +%Y%m%d%H%M%S`".log"``
  <br>
  Digunakan untuk membuat nama file dengan ketentuan YmdHms yang disimpan ke variabel *file*

- ``mem=$(awk '$1=="Mem:" {printf "%s,%s,%s,%s,%s,%s", $2, $3, $4, $5, $6, $7}' <<< `free -m`)``
  <br>
  Program AWK ini akan membaca output dari command free -m pada bagian **mem**. Kemudian output dari free -m berupa mem total, mem used, mem free, mem shared, mem buff/cache, dan mem available akan disimpan oleh AWK ke variabel *mem*

- ``swap=$(awk '$1=="Swap:" {printf "%s,%s,%s", $2, $3, $4}' <<< `free -m`)``
  <br>
  Program AWK ini akan membaca output dari command free -m pada bagian **swap**. Kemudian output dari free -m berupa swap total, swap used, dan swap free akan disimpan oleh AWK ke variabel *swap*

- ``dir=$(awk '{printf "%s", $1}' <<< `du -sh /home/"$user"`)``
  <br>
  Program AWK ini akan membaca output dari command du -sh pada argumen pertama. Kemudian output dari du -sh berupa hasil monitoring size suatu directory akan disimpan oleh AWK ke variabel *dir*

- `echo "mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> /home/"$user"/log/"$file"`
  <br>
  Bagian ini digunakan untuk menampilkan mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size pada file log sesuai dengan format directory

- `echo "$mem,$swap,/home/$user/,$dir" >> /home/"$user"/log/"$file"`
  <br>
  Bagian ini digunakan untuk menampilkan isi dari variabel *mem*, *swap*, dan *dir* pada file log
<br>

> Output metrics minute_log.sh
![minute_log](img/minutes_log.png)

<br>

### b. Membuat script untuk mencatat metrics secara otomatis tiap menit

![3b](img/3_cron.png)

- `* * * * * /home/arcadium/soal-shift-sisop-modul-1-ITB04-2022/soal3/minute_log.sh`
  <br>
  Cronjob pada bagian ini digunakan untuk menjalankan file **minute_log.sh** setiap menit

- `0 * * * * /home/arcadium/soal-shift-sisop-modul-1-ITB04-2022/soal3/aggregate_minutes_to_hourly_log.sh`
  <br>
  Cronjob pada bagian ini digunakan untuk menjalankan file **aggregate_minutes_to_hourly_log.sh** setiap jam

<br>

### c. Membuat agregasi file log ke satuan jam
``` sh
#!/bin/bash

user=$(whoami)
list=($(awk '{print}' <<< `ls -r -I 'metrics_agg_*' "/home/$user/log/" | head -n 60`))

for i in "${list[@]}";
do
	path="/home/$user/log/$i";
        mem_total+=($(awk -F, 'NR==2 {print $1}' "$path"));
	mem_total_avg=$(("$mem_total_avg" + $(awk -F, 'NR==2 {print $1}' "$path")));
	mem_used+=($(awk -F, 'NR==2 {print $2}' "$path"));
	mem_used_avg=$(("$mem_used_avg" + $(awk -F, 'NR==2 {print $2}' "$path")));
	mem_free+=($(awk -F, 'NR==2 {print $3}' "$path"));
	mem_free_avg=$(("$mem_free_avg" + $(awk -F, 'NR==2 {print $3}' "$path")));
	mem_shared+=($(awk -F, 'NR==2 {print $4}' "$path"));
	mem_shared_avg=$(("$mem_shared_avg" + $(awk -F, 'NR==2 {print $4}' "$path")));
	mem_buff+=($(awk -F, 'NR==2 {print $5}' "$path"));
	mem_buff_avg=$(("$mem_buff_avg" + $(awk -F, 'NR==2 {print $5}' "$path")));
	mem_available+=($(awk -F, 'NR==2 {print $6}' "$path"));
	mem_available_avg=$(("$mem_available_avg" + $(awk -F, 'NR==2 {print $6}' "$path")));
	swap_total+=($(awk -F, 'NR==2 {print $7}' "$path"));
        swap_total_avg=$(("$swap_total_avg" + $(awk -F, 'NR==2 {print $7}' "$path")));
	swap_used+=($(awk -F, 'NR==2 {print $8}' "$path"));
        swap_used_avg=$(("$swap_used_avg" + $(awk -F, 'NR==2 {print $8}' "$path")));
	swap_free+=($(awk -F, 'NR==2 {print $9}' "$path"));
	swap_free_avg=$(("$swap_free_avg" + $(awk -F, 'NR==2 {print $9}' "$path")));
        path_dir=$(awk -F, 'NR==2 {print $10}' "$path");
	path_size+=($(awk -F'[M,]' 'NR==2 {print $11}' "$path"));
	path_size_avg=$(("$path_size_avg" + $(awk -F'[M,]' 'NR==2 {print $11}' "$path")));
done

mem_total_avg=$(("$mem_total_avg"/60))
mem_used_avg=$(("$mem_used_avg"/60))
mem_free_avg=$(("$mem_free_avg"/60))
mem_shared_avg=$(("$mem_shared_avg"/60))
mem_buff_avg=$(("$mem_buff_avg"/60))
mem_available_avg=$(("$mem_available_avg"/60))
swap_total_avg=$(("$swap_total_avg"/60))
swap_used_avg=$(("$swap_used_avg"/60))
swap_free_avg=$(("$swap_free_avg"/60))
path_size_avg=$(("$path_size_avg"/60))

mem_total_sorted=($(for i in "${mem_total[@]}"; do echo "$i"; done | sort -n))
mem_used_sorted=($(for i in "${mem_used[@]}"; do echo "$i"; done | sort -n))
mem_free_sorted=($(for i in "${mem_free[@]}"; do echo "$i"; done | sort -n))
mem_shared_sorted=($(for i in "${mem_shared[@]}"; do echo "$i"; done | sort -n))
mem_buff_sorted=($(for i in "${mem_buff[@]}"; do echo "$i"; done | sort -n))
mem_available_sorted=($(for i in "${mem_available[@]}"; do echo "$i"; done | sort -n))
swap_total_sorted=($(for i in "${swap_total[@]}"; do echo "$i"; done | sort -n))
swap_used_sorted=($(for i in "${swap_used[@]}"; do echo "$i"; done | sort -n))
swap_free_sorted=($(for i in "${swap_free[@]}"; do echo "$i"; done | sort -n))
path_size_sorted=($(for i in "${path_size[@]}"; do echo "$i"; done | sort -n))

file=$(printf "metrics_agg_%(%Y%m%d%H)T\n" $(( $(printf "%(%s)T") - 60 * 60 )))
file_dir="/home/$user/log/$file"".log"

echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$file_dir"
echo "minimum,${mem_total_sorted[0]},${mem_used_sorted[0]},${mem_free_sorted[0]},${mem_shared_sorted[0]},${mem_buff_sorted[0]},${mem_available_sorted[0]},${swap_total_sorted[0]},${swap_used_sorted[0]},${swap_free_sorted[9]},$path_dir,${path_size_sorted[0]}M" >> "$file_dir"
echo "maximum,${mem_total_sorted[59]},${mem_used_sorted[59]},${mem_free_sorted[59]},${mem_shared_sorted[59]},${mem_buff_sorted[59]},${mem_available_sorted[59]},${swap_total_sorted[59]},${swap_used_sorted[59]},${swap_free_sorted[59]},$path_dir,${path_size_sorted[59]}M" >> "$file_dir"
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$path_dir,$path_size_avg""M" >> "$file_dir"
```
- `user=$(whoami)`
  <br>
  Digunakan untuk mendapatkan username yang disimpan ke dalam variabel *user*

- ``list=($(awk '{print}' <<< `ls -r -I 'metrics_agg_*' "/home/$user/log/" | head -n 60`))``
  <br>
  Digunakan untuk mengambil list dari 60 file terakhir dan `ls -r -I 'metrics_agg_` digunakan untuk menghindari file dengan format nama seperti di samping

- `for i in "${list[@]}";`
  <br>
  Pada bagian for loop ini digunakan untuk mengambil data tiap metrics yang diminta. Kemudian masing masing data akan disimpan pada satu variabel

``` sh
mem_total_avg=$(("$mem_total_avg"/60))
mem_used_avg=$(("$mem_used_avg"/60))
mem_free_avg=$(("$mem_free_avg"/60))
mem_shared_avg=$(("$mem_shared_avg"/60))
mem_buff_avg=$(("$mem_buff_avg"/60))
mem_available_avg=$(("$mem_available_avg"/60))
swap_total_avg=$(("$swap_total_avg"/60))
swap_used_avg=$(("$swap_used_avg"/60))
swap_free_avg=$(("$swap_free_avg"/60))
path_size_avg=$(("$path_size_avg"/60))
```
Bagian ini digunakan untuk memperoleh rata-rata dari metrics setiap menit dalam 1 jam

``` sh
mem_total_sorted=($(for i in "${mem_total[@]}"; do echo "$i"; done | sort -n))
mem_used_sorted=($(for i in "${mem_used[@]}"; do echo "$i"; done | sort -n))
mem_free_sorted=($(for i in "${mem_free[@]}"; do echo "$i"; done | sort -n))
mem_shared_sorted=($(for i in "${mem_shared[@]}"; do echo "$i"; done | sort -n))
mem_buff_sorted=($(for i in "${mem_buff[@]}"; do echo "$i"; done | sort -n))
mem_available_sorted=($(for i in "${mem_available[@]}"; do echo "$i"; done | sort -n))
swap_total_sorted=($(for i in "${swap_total[@]}"; do echo "$i"; done | sort -n))
swap_used_sorted=($(for i in "${swap_used[@]}"; do echo "$i"; done | sort -n))
swap_free_sorted=($(for i in "${swap_free[@]}"; do echo "$i"; done | sort -n))
path_size_sorted=($(for i in "${path_size[@]}"; do echo "$i"; done | sort -n))
```
Pada bagian ini digunakan untuk melakukan sorting pada program sehingga bisa didapatkan nilai minimum dan maximum

- `file=$(printf "metrics_agg_%(%Y%m%d%H)T\n" $(( $(printf "%(%s)T") - 60 * 60 )))`
  <br>
  Bagian ini digunakan untuk menyimpan penamaan file satu jam sebelumnya. Penamaan file tersebut disimpan dalam variabel bernama *file*

``` sh
echo "type,mem_total,mem_used,mem_free,mem_shared,mem_buff,mem_available,swap_total,swap_used,swap_free,path,path_size" >> "$file_dir"
echo "minimum,${mem_total_sorted[0]},${mem_used_sorted[0]},${mem_free_sorted[0]},${mem_shared_sorted[0]},${mem_buff_sorted[0]},${mem_available_sorted[0]},${swap_total_sorted[0]},${swap_used_sorted[0]},${swap_free_sorted[9]},$path_dir,${path_size_sorted[0]}M" >> "$file_dir"
echo "maximum,${mem_total_sorted[59]},${mem_used_sorted[59]},${mem_free_sorted[59]},${mem_shared_sorted[59]},${mem_buff_sorted[59]},${mem_available_sorted[59]},${swap_total_sorted[59]},${swap_used_sorted[59]},${swap_free_sorted[59]},$path_dir,${path_size_sorted[59]}M" >> "$file_dir"
echo "average,$mem_total_avg,$mem_used_avg,$mem_free_avg,$mem_shared_avg,$mem_buff_avg,$mem_available_avg,$swap_total_avg,$swap_used_avg,$swap_free_avg,$path_dir,$path_size_avg""M" >> "$file_dir"
```
Bagian ini digunakan untuk menampilkan hasil agregasi

<br>

> Output metrics aggregate_minutes_to_hourly_log.sh
![agg_hourly](img/agg.png)

<br>

### d. Memastikan semua file log hanya dapat dibaca oleh user pemilik file
- `chmod 700 "$file_dir"`
  <br>
  Bagian ini digunakan agar file log hanya dapat dibaca oleh user pemilik file

> Output soal 3d

![soal_d](img/3d.png)

<br>

<br>

# Kendala Pengerjaan

- Pada saat meng-execute script dari soal nomor 3c, yaitu **aggregate_minutes_to_hourly_log.sh** sempat muncul error, seperti gambar di bawah ini
![error_agg](img/error.png)

- Praktikan belum mengetahui keyword yang tepat dalam mencari referensi sehingga dalam pengerjaan soal seringkali terhambat
