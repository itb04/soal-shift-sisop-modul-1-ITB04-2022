#!/bin/bash

echo "Input username and password to login!"
read -p "Username: " username
read -p "Password: " -s password
echo -e

exist=$(awk -v username="${username}" -v password="${password}" '$1==username && $2==password {print 1}' ./users/user.txt)

d=`date +%D`
t=`date +%T`

if [[ "$exist" == 1 ]]
then
	echo "$d $t LOGIN: INFO User $username logged in" >> log.txt
	read command
	cmd=$(awk '{print $1}' <<< "$command") 
	q=$(awk '{print $2}' <<< "$command")
	if [[ "$cmd" == "dl" ]]
	then
		sudo apt install zip unzip
		folder=`date +%F`"_$username"
		index=0
		if [[ ! -f "./$folder.zip" ]]
		then
			mkdir -p "./$folder"
		else
			unzip -P "$password" "./$folder.zip"
			file=`ls "$folder" -r | head -n 1`
			index=$(awk -F"_" '{print $2}' <<< "$file")
		fi
		for ((num=index+1; num<=index+q; num++))
		do
			wget -O "$folder/PIC_$num" https://loremflickr.com/320/240
		done
		zip -r -P "$password" "$folder.zip" "$folder"
		rm -rf "$folder"
	elif [[ "$cmd" == "att" ]]
	then 
		awk -v username="${username}" '$3=="LOGIN:" && ($6==username || $10==username) {++n} END {print "Login attempt sebanyak", n, "kali"}' log.txt
	fi
else
	echo "$d $t LOGIN: ERROR Failed login attempt on user $username" >> log.txt
	exit
fi
