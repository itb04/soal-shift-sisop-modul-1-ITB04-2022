#!/bin/bash

mkdir -p ./users
test -f ./users/user.txt || touch ./users/user.txt
test -f log.txt || touch log.txt

printf "Input username and password to register!\n"
read -p "Username: " username

exist=$(awk -v username="${username}" '$1==username {print 1}' ./users/user.txt)

d=`date +%D`
t=`date +%T`

if [[ "$exist" == 1 ]]
then
	echo "$d $t REGISTER: ERROR User already exist" >> log.txt
	exit
fi

read -p "Password: " -s password

if [[ "$password" != "$username" && "${#password}" -ge 8 && "$password" == *[A-Z]* && "$password" == *[a-z]* && "$password" == *[0-9]* ]]
then
	echo "$d $t REGISTER: INFO User $username registered successfully" >> log.txt
	echo "$username $password" >> ./users/user.txt
	exit
else
	echo "$d $t REGISTER: ERROR password does not match the format" >> log.txt
	exit
fi
